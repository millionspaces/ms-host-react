var fs = require('fs');
var shortid = require('shortid');
var moment = require('moment');

console.log('Generating build info..');

fs.readFile('src/metadata.qa.json', function (err, content) {

    var metadata = JSON.parse(content);

    var newMetaData = {
        "build": shortid.generate(), 
        "timestamp": Date.now(),
        "time": moment().format('MMMM Do YYYY, h:mm:ss a')
    };

    metadata.push(newMetaData);

    fs.writeFile('src/metadata.qa.json',JSON.stringify(metadata),function(err){
        if(err) throw err;
        console.log('Latest Build: ', newMetaData);
    });
})

