# MS Host Space Edit App Release Notes.

# v1.0.0.0

- #### v1.1.12.0
    * 27th April 2018
        *   FEATURE: All the features, comments, docs and bugfixes cloned from ms-admin.
      
      
- #### v1.1.11.0
    * 27th April 2018
        *   FEATURE: Implementing host details component; Cloning document, bug fixes, improvements and features from ms admin upto version v1.0.7.17.
      

- #### v1.1.10.0
    * 26th April 2018
        *   FEATURE: Implementing Details and accessibility component; Cloning all document, bug fixes, improvements and features from ms admin upto version v1.0.7.17.
      

- #### v1.1.9.0
    * 25th April 2018
        *   FEATURE: Implementing MenuFIles component; Cloning all bug fixes, improvements and features from ms admin upto version v1.0.7.17.
        

- #### v1.1.8.0
    * 25th April 2018
        *   REVAMP: Space rules Component, Cloning all bug fixes, improvements and features from ms admin upto version v1.0.7.17.
        

- #### v1.1.7.0
    * 23rd April 2018
        *   REVAMP: Cancellation policy Component, Cloning all bug fixes, improvements and features from ms admin upto version. v1.0.7.17.


- #### v1.1.6.0
    * 19th April 2018
        *   REVAMP: Bank Detials Component, Cloning all bug fixes, improvements and features from ms admin upto version v1.0.7.17.


- #### v1.1.5.0
    * 19th April 2018
        *   REVAMP: Basic space info component, Cloning all bug fixes, improvements and features from ms admin upto version v1.0.7.17.
        *   REVAMP: Space list, Cloning all bug fixes, improvements and features from ms admin upto version v1.0.7.17.

- #### v1.1.4.0
    * 19th April 2018
        *   REVAMP: Host logo edit, Moving all bug fixes, improvements and features from ms admin upto version v1.0.7.17.
 

- #### v1.1.3.0

    * 14th Mar 2018
        
        *   QA Build: HyqGUY8Kz
        *   QA Build Timestamp (UTC): 1521026577785

    * 13th Mar 2018

        *   FEATURE: Host Logo edit section.
        *   FEATURE: Host details edit section.
        *   FEATURE: Basic space info edit section.
        *   FEATURE: Space Details and Accessibility edit section.
        *   FEATURE: Space Rules edit section.
        *   FEATURE: Cancellation Policy edit section.
        *   FEATURE: Bank Details edit section.
        *   FEATURE: Space Images edit section.
        *   FEATURE: Build and deploy 


- #### v1.1.2.0

    * 7th Mar 2018

        *   DESIGN: Added ant design principles and components.
        *   FEATURE: Login Module was added.


- #### v1.1.1.0

    * 7th Mar 2018

        *   INIT: Initializing project.
        *   INIT: We used create-react-app to scaffold the App...
