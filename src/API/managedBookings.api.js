import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * API Call: Fetch managed bookings.
 */

export const fetchManagedBookingsByPage = pageNumber => {
    return fetch (`${API}admin/booking/new/${pageNumber}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
}

export const fetchABooking = bookingId => {
    return fetch (`${API}book/spaces`, {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            isManual: false,
            bookingId
        })
    });
}