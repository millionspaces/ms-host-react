// TODO: REFACT DOC

import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * API Call: 
 */

export const fetchTentetiveBooking = pageNumber => {
    return fetch (`${API}admin/tentative/${pageNumber}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
};

export const confirmTentetiveBooking = bookingId => {
    return fetch (`${API}book/space`, {
        method: 'PUT',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            booking_id: bookingId,
            event: 1,
            method: "MANUAL",
            status: 2
        })
    });        
}