import { API_SETTINGS } from './CONSTS';

const { API } = API_SETTINGS;

/**
 * API Call: Requesting all the spaces
 * @param {Number} page 
 */
export const fetchSpacesSet = page => {
    return fetch (`${API}admin/space/new/${page}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
};


/**
 * API Call: Toggle Space active status
 * @param {Number} id Space Id
 */
export const toggleSpaceStatus = id => {
    return fetch (`${API}admin/space/acceptance`,  {
        method: 'POST',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({id})
    });
}

/**
 * API Call: Fetch Space by id.
 */

 export const fetchSpaceById = id => {
    return fetch (`${API}space/${id}`, {
        method: 'GET',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        }
    });
 }


/**
 * API Call: Update Space Details.
 * @param {Object} space 
 */
export const updateSpaceDetails = space => {
    return fetch (`${API}space/${space.id}`, {
        method: 'PUT',
        credentials: 'include',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(space)
    }).then(res => res.json());
}

/**
 * Get all the spaces for a user 
 * @param {Object} user User id
 * @returns {Promise}
 */
export const fetchAllSpacesOfUser = user => {
    return fetch(
        `${API}user/spaces`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }
    )
};

