import {

    COMMON__FETCHING_SEATING_ARRANGEMENTS,
    COMMON__FETCHING_SEATING_ARRANGEMENTS_SUCCESS,
    COMMON__FETCHING_SEATING_ARRANGEMENTS_FAILED,

    COMMON__FETCHING_ACTIVITIES,
    COMMON__FETCHING_ACTIVITIES_SUCCESS,
    COMMON__FETCHING_ACTIVITIES_FAILED,

    COMMON__FETCHING_AMENITIES,
    COMMON__FETCHING_AMENITIES_SUCCESS,
    COMMON__FETCHING_AMENITIES_FAILED,

    COMMON__FETCHING_AMENITY_UNITS,
    COMMON__FETCHING_AMENITY_UNITS_SUCCESS,
    COMMON__FETCHING_AMENITY_UNITS_FAILED,
    
    COMMON__FETCHING_SPACE_TYPES,
    COMMON__FETCHING_SPACE_TYPES_SUCCESS,
    COMMON__FETCHING_SPACE_TYPES_FAILED,

    COMMON__FETCHING_BLOCK_CHARGE_TYPES,
    COMMON__FETCHING_BLOCK_CHARGE_TYPES_SUCCESS,
    COMMON__FETCHING_BLOCK_CHARGE_TYPES_FAILED,

    COMMON__FETCHING_SPACE_RULES,
    COMMON__FETCHING_SPACE_RULES_SUCCESS,
    COMMON__FETCHING_SPACE_RULES_FAILED,

    COMMON__FETCHING_CANCELLATION_POLICIES,
    COMMON__FETCHING_CANCELLATION_POLICIES_SUCCESS,
    COMMON__FETCHING_CANCELLATION_POLICIES_FAILED

} from '../actionTypes/spaceCommon';

import { fetchSpaceRules, fetchAllSeatingArrangements, fetchAllEventTypes, fetchAllAmenities, fetchAmenityUnits, fetchSpaceTypes, fetchBlockChargeTypes, fetchCancellationPolicies } from '../../API/common';


/**
 * Creates actions for fetching seating arrangements.
 */
export const getAllSeatingArrangements = () => {

    return dispatch => {

        dispatch ({
            type: COMMON__FETCHING_SEATING_ARRANGEMENTS
        });

        fetchAllSeatingArrangements()
            .then(data => {
                dispatch({
                    type: COMMON__FETCHING_SEATING_ARRANGEMENTS_SUCCESS,
                    payload: data
                });    
            })
            .catch(err => {
                dispatch ({
                    type: COMMON__FETCHING_SEATING_ARRANGEMENTS_FAILED,
                    payload: err
                });
            });

    }
}

/**
 * Creates actions for fetching Activities.
 */
export const getAllActivities = () => {

    return dispatch => {
        dispatch ({
            type: COMMON__FETCHING_ACTIVITIES
        });
    
        fetchAllEventTypes()
            .then( data => {
                dispatch({
                    type: COMMON__FETCHING_ACTIVITIES_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch({
                    type: COMMON__FETCHING_ACTIVITIES_FAILED,
                    payload: err
                });
            });
    }
}

/**
 * Creates actions for fetching Amenities.
 */
export const getAllAmenities = () => {
    return dispatch => {
        dispatch ({
            type: COMMON__FETCHING_AMENITIES
        });

        fetchAllAmenities()
            .then ( data => {
                dispatch ({
                    type: COMMON__FETCHING_AMENITIES_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: COMMON__FETCHING_AMENITIES_FAILED,
                    payload: err
                })
            });
     }
}

/**
 * Creates actions for fetching amenity units.
 */
export const getAllAmenityUnits = () => {
    return dispatch => {

        dispatch({
            type: COMMON__FETCHING_AMENITY_UNITS
        });

        fetchAmenityUnits()
            .then (data => {
                dispatch ({
                    type: COMMON__FETCHING_AMENITY_UNITS_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: COMMON__FETCHING_AMENITY_UNITS_FAILED,
                    payload: err
                });
            });
    }
}

/**
 * Creates actions for fetching space types.
 */
export const getSpaceTypes = () => {
    return dispatch => {

        dispatch ({
            type: COMMON__FETCHING_SPACE_TYPES
        });

        fetchSpaceTypes()
            .then( data => {
                dispatch({
                    type: COMMON__FETCHING_SPACE_TYPES_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch({
                    type: COMMON__FETCHING_SPACE_TYPES_FAILED,
                    payload: err
                });
            });
    }
}

export const getBlockChargeTypes = () => {
    return dispatch => {
        
        dispatch ({
            type: COMMON__FETCHING_BLOCK_CHARGE_TYPES
        });

        fetchBlockChargeTypes ()
            .then( data => {
                dispatch ({
                    type: COMMON__FETCHING_BLOCK_CHARGE_TYPES_SUCCESS,
                    payload: data
                });
            })
            .catch (err => {
                dispatch({
                    type: COMMON__FETCHING_BLOCK_CHARGE_TYPES_FAILED,
                    payload: err
                });
            });
    };
}

export const getSpaceRules = () => {
    return dispatch => {
        dispatch({
            type: COMMON__FETCHING_SPACE_RULES
        });

        fetchSpaceRules()
            .then( data => {
                dispatch ({
                    type: COMMON__FETCHING_SPACE_RULES_SUCCESS,
                    payload: data
                })
            })
            .catch (err => {
                dispatch({
                    type: COMMON__FETCHING_SPACE_RULES_FAILED,
                    payload: err
                });
            });

    }
}

export const getCancellationPolicies = () => {
    return dispatch => {


        dispatch ({
            type: COMMON__FETCHING_CANCELLATION_POLICIES
        });

        fetchCancellationPolicies()
            .then (data => {
                dispatch ({
                    type: COMMON__FETCHING_CANCELLATION_POLICIES_SUCCESS,
                    payload: data
                });
            })
            .catch ( err => {
                dispatch ({
                    type: COMMON__FETCHING_CANCELLATION_POLICIES_FAILED,
                    payload: err
                })
            })
    }
}