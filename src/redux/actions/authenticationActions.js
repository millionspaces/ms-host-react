import {
    AUTHENTICATION__ATTEMPTING_TO_LOGIN,
    AUTHENTICATION__LOGIN_SUCCESS,
    AUTHENTICATION__LOGIN_FAILED,
    AUTHENTICATION__ATTEMPTING_TO_LOGOUT,
    AUTHENTICATION__LOGOUT_SUCCESS,
    AUTHENTICATION__LOGOUT_FAILED,
} from '../actionTypes/authenticationActionTypes';

import { loginUserDevice, userDeviceLogout } from '../../API/authentication';

/**
 * Creates actions for user device login.
 * @param {Object} user Describes user to be logged in
 */
export const userDeviceLoginActionCreator = user => {
    return dispatch => {
        dispatch ({
            type: AUTHENTICATION__ATTEMPTING_TO_LOGIN
        });

        loginUserDevice(user)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: AUTHENTICATION__LOGIN_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: AUTHENTICATION__LOGIN_FAILED,
                    payload: err
                });
            });
    }
};


/**
 * Creates actions for user device logout.
 */
export const userDeviceLogoutActionCreator = () => {

    return dispatch => {
        
        dispatch ({
            type: AUTHENTICATION__ATTEMPTING_TO_LOGOUT
        });
        
        userDeviceLogout().then(res => res.json).then(data => {
            dispatch ({
                type: AUTHENTICATION__LOGOUT_SUCCESS
            });
        }).catch(err => {
            dispatch({
                type: AUTHENTICATION__LOGOUT_FAILED,
                payload: err
            });
        });
    }
    
}