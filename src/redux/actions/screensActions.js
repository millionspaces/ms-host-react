import { SCREENS__SET_MAIN_SCREEN } from "../actionTypes/screenActionTypes";

export const setCurrentScreen = screen => ({
    type: SCREENS__SET_MAIN_SCREEN,
    payload: screen
});