import { fetchManagedBookingsByPage, fetchABooking } from "../../API/managedBookings.api";

import { 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS, 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS, 
    MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED, 
    MANAGED_BOOKINGS__FETCHING_A_BOOKING,
    MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS,
    MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED
} from "../actionTypes/managedBookingsActionTypes";

/**
 * Creates actions for retreving managed bookings
 * @param {string} pageNumber Page number
 */
 export const getMangedBookings = pageNumber => {
    return dispatch => {

        dispatch ({
            type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS
        });
        
        fetchManagedBookingsByPage (pageNumber)
            .then(res => res.json())
            .then(data => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_MANGED_BOOKINGS_FAILED,
                    payload: err
                })
            });
    }
}

/**
 * Creates actions for retreving managed bookings
 * @param {string} bookingId Page number
 */
export const getBookingDetailsById = bookingId => {

    console.log(bookingId)
    return dispatch => {
        dispatch({
            type: MANAGED_BOOKINGS__FETCHING_A_BOOKING
        });

        fetchABooking (bookingId)
            .then (res => res.json())
            .then (data => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_A_BOOKING_SUCCESS,
                    payload: data
                });
            })
            .catch(err => {
                dispatch ({
                    type: MANAGED_BOOKINGS__FETCHING_A_BOOKING_FAILED,
                    payload: err
                })
            });
    }
}
