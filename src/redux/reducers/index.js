/**
 * This file imports all the reducers and exports single reducer object
 */
import { combineReducers } from 'redux';

import { userDeviceLoginReducer } from './authentication';
import { spacesReducer, spaceToggleReducer, spaceDetailsReducer } from './spacesReducer';
import { spacesRulesReducer, seatingArrangementsReducer, activitiesReducer, amenitiesReducer, amenityUnitsReducer, spaceTypesReducer, blockChargeTypesReducer, cancellationPoliciesReducer } from './commonReducers';
import { hostLogoDataReducer } from './spaceImagesReducer';

const reducers = combineReducers({
    auth: userDeviceLoginReducer,
    spaces: spacesReducer,
    spaceToggle: spaceToggleReducer,
    space: spaceDetailsReducer,
    seatingArrangements: seatingArrangementsReducer,
    activities: activitiesReducer,
    amenities: amenitiesReducer,
    amenityUnits: amenityUnitsReducer,
    spaceTypes: spaceTypesReducer,
    blockChargeTypes: blockChargeTypesReducer,
    spaceRules: spacesRulesReducer,
    cancellationPolicies: cancellationPoliciesReducer,
    hostLogoData: hostLogoDataReducer
});

export default reducers;