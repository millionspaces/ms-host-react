// TODO: DOC
import {
    SPACES__FETCHING_SPACES_SET,
    SPACES__RECEIVING_SPACES_SET_SUCCESS,
    SPACES__RECEIVING_SPACES_SET_FAILED,

    SPACE__TOGGLING_SPACE_APPROVAL_STATE,
    SPACE__TOGGLING_SPACE_APPROVAL_STATE_SUCCESS,
    SPACE__TOGGLING_SPACE_APPROVAL_STATE_FAILED,

    SPACE__FETCHING_SPACE_DETAILS,
    SPACE__FETCHING_SPACE_DETAILS_SUCCESS,
    SPACE__FETCHING_SPACE_DETAILS_FAILED,

    SPACE_SAVE__SAVING_SPACE_DETAILS,
    SPACE_SAVE__SAVING_SPACE_DETAILS_SUCCESS,
    SPACE_SAVE__SAVING_SPACE_DETAILS_FAILED,

    SPACE__CLEAR_SPACE_DETAILS

} from '../actionTypes/spacesActionTypes';

export const spacesReducer = (state={fetching: false, error: {status: false, message:""}, data: []}, action) => {

    switch (action.type) {

        case SPACES__FETCHING_SPACES_SET:
            return Object.assign({}, state, {fetching: true});


        case SPACES__RECEIVING_SPACES_SET_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case SPACES__RECEIVING_SPACES_SET_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message: "Unable to receive spaces"}});

        
        default:
            return state;
    }
}

export const spaceToggleReducer = (state={fetching: false, error: {status: false, message:""}}, action) => {
    switch (action.type) {
        
        case SPACE__TOGGLING_SPACE_APPROVAL_STATE:
            return Object.assign({}, state, {fetching: true});

        case SPACE__TOGGLING_SPACE_APPROVAL_STATE_SUCCESS:
            return Object.assign({}, {fetching: false}, {id: action.payload.id});

        case SPACE__TOGGLING_SPACE_APPROVAL_STATE_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message:"Failed to toggle"}});

        default:
            return state;
    }
}

export const spaceDetailsReducer = (state={
    fetching: false,
    error: {status: false, message:""},
    data: [],
    updateSuccess: false
}, action) => {
    switch (action.type) {
        case SPACE__FETCHING_SPACE_DETAILS:
            return Object.assign({}, state, {fetching: true});

        case SPACE__FETCHING_SPACE_DETAILS_SUCCESS:
            return Object.assign({}, {fetching: false}, {data: action.payload});

        case SPACE__FETCHING_SPACE_DETAILS_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message:"Failed to Fetch Space Details"}});
        
        case SPACE_SAVE__SAVING_SPACE_DETAILS:
            return Object.assign({}, state, {fetching: true}, {updateSuccess: false});

        case SPACE_SAVE__SAVING_SPACE_DETAILS_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {updateSuccess: true});

        case SPACE_SAVE__SAVING_SPACE_DETAILS_FAILED:
            return Object.assign({}, state, {fetching: false}, {error: {status: true, message:"Failed to update Space Details"}});
        
        case SPACE__CLEAR_SPACE_DETAILS:
            return state={
                fetching: false,
                error: {status: false, message:""},
                data: [],
                updateSuccess: false
            }
        default:
            return state;
    }
}
