import { SCREENS__SET_MAIN_SCREEN } from "../actionTypes/screenActionTypes";

export const screensReducer = (
    state={
        mainScreen: 'Home',
        subScreen: ''
    }, action
) => {
    switch (action.type) {
        case SCREENS__SET_MAIN_SCREEN:
            return Object.assign ({}, state, { mainScreen: action.payload});

        default:
            return state;
    }
}