import { SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA, SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_SUCCESS } from "../actionTypes/spaceImagesActionTypes";

export const hostLogoDataReducer = (state={
    fetching: false,
    error: {status: false, message: ""},
    data: []
}, action) => {
    switch (action.type) {
        case SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA:
            return Object.assign({}, state, {fetching: true});

        case SPACE_IMAGES__FETCHING_SPACE_IMAGE_DATA_SUCCESS:
            return Object.assign({}, state, {fetching: false}, {data: action.payload});
            
        default:
            return state;
    }
}