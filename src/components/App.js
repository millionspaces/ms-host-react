import React, { Component } from 'react';
import PropTypes, { instanceOf } from 'prop-types';
import './App.css';

// Custom components
import Dashboard from './dashboard/Dashboard';
import LoginModal from './login/LoginModal';

// Other
import {Cookies, withCookies } from 'react-cookie';
import { compose } from 'recompose';

// Redux
import { connect } from 'react-redux';

/**
 * Class Representing App
 */
class App extends Component {

	static propTypes = {
		cookies: instanceOf(Cookies).isRequired,
		id: PropTypes.number,
		role: PropTypes.string
	}

	state = {
		loggedIn: false,
		fetching: false
	};

	componentDidMount () {
		
		const { cookies } = this.props;

		cookies.get('MS_ADMIN_USER')?this.setState({loggedIn: true}):this.setState({loggedIn: false})

	}

	componentWillReceiveProps (nextProps) {

		const { id, fetching, role, cookies, logged } = nextProps;
	
		if (fetching) {
			this.setState({
				fetching
			});
		}
		if (logged && id && role === 'ADMIN') {
			cookies.set('MS_ADMIN_USER', id, {path:"/"});
			this.setState({loggedIn: true});
		}

		if (!logged) {
			cookies.remove('MS_ADMIN_USER', {path: '/'});
			this.setState({ loggedIn: false, fetching });
		}

	}

	render () {

		let { loggedIn, fetching } = this.state;

		return (
			<div className="main-wrapper">
				{loggedIn?<Dashboard />:<LoginModal fetching={fetching} loggedIn={loggedIn} />}
			</div>
		);
	}
}

/**
 * Maps Redux store to component props
 */
const mapStateToProps = ({auth}) => {

	const {id, role, fetching, name, logged} = auth;

	return {
		id,
		role,
		fetching,
		logged,
		name
	}
}

App = compose(
	connect(mapStateToProps, null)
) (App);

export default withCookies (App);
