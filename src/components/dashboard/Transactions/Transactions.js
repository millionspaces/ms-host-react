import React from 'react';
import { Layout, Table, Pagination, Icon, Button, Upload, message, Popconfirm, Tooltip, Badge } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { getTransactionDetails, setTransactionProof } from '../../../redux/actions/transactionActions';
import { PDF__TRANSACTIONS_CLOUDE_NAME, PDF__TRANSACTIONS_PRESET, DATE_FORMAT } from '../../../settings';
import { setCurrentScreen } from '../../../redux/actions/screensActions';

const { Content } = Layout;

/**
 * Class representing Transactions Component.
 *  
 */
class Transactions extends React.Component {

    state = {
        currentPage: 1
    }

    componentDidMount () {
        // Once the component is mounted then fetch first 10 transaction details.
        this.props.getTransactionDetails(this.state.currentPage-1);
        this.props.setScreen('Transactions')
    }
    
    /**
     * Triggers on pagination changes.
     * @param {Number} pageNumber Requested page number.
     */
    onPaginationChange = (pageNumber) => {

        this.props.getTransactionDetails(pageNumber-1);

        this.setState({
            currentPage: pageNumber
        });
    }

    /**
     * Opens Cloudinary widget and with pdf upload wizard.
     * @param {Object} booking Describes a booking
     */
    openWidgetForThumbnail = booking => {
        window.cloudinary.openUploadWidget({
            cloud_name: PDF__TRANSACTIONS_CLOUDE_NAME,
            upload_preset: PDF__TRANSACTIONS_PRESET,
            theme: 'white',
            folder: 'transactions',
            client_allowed_formats: ["pdf", "jpeg", "jpg"],
        }, (error, result) => {
            if (!error) {
                this.props.setTransactionProof({
                    pdf: result[0].url,
                    bookingId: booking.id,
                    verify: 1
                }, this.state.currentPage-1);
            }
        });
    }

    render () {

        // Transaction details column table header.
        const columns = [{
            title: 'Ref',
            dataIndex: 'orderId',
            key: 'orderId',
            width: 100
        }, {
            title: 'Space Name',
            dataIndex: 'spaceName',
            key: 'spaceName',
            width: 180
        }, {
            title: 'Organization',
            dataIndex: 'organizationName',
            key: 'organizationName'
        }, {
            title: 'Event Date',
            key: 'eventDate',
            dataIndex: 'eventDate'
        }, {
            title: 'Cancellation Policy',
            key: 'cancellationPolicy',
            dataIndex: 'cancellationPolicy'
        }, {
            title: 'Cut Off Date',
            key: 'cancellationCutOffDate',
            dataIndex: 'cancellationCutOffDate'
        }, {
            title: '%',
            key: 'commissionPercentage',
            dataIndex: 'commissionPercentage',
            width: 50
        }, {
            title: 'Total',
            key: 'total',
            dataIndex: 'total'
        }, {
            title: 'Host payment (LKR)',
            key: 'hostCharge',
            dataIndex: 'hostCharge'
        }, {
            title: 'Payment Due Date',
            key: 'paymentDueDate',
            dataIndex: 'paymentDueDate',
            render: paymentDueDate => <Tooltip placement="bottom" title={paymentDueDate.tooltip}><span>{paymentDueDate.text}</span></Tooltip>
        }, {
            title: <div style={{textAlign: 'center'}}><Badge overflowCount={999} offset={[-8, 8]} count={this.props.pendingTransactions}>Verify</Badge></div>,
            dataIndex: 'upload',
            key: 'upload',
            width: 120,
            render: (text, rec) => !rec.paymentVerified
                ?
                    <Popconfirm placement="left" title="Are you sure you want to verify the payment?" onConfirm={() => this.openWidgetForThumbnail(rec)} okText="Yes" cancelText="No">
                        <Tooltip placement="left" title="Click to upload payment proof.">
                            <Button size="small" loading={this.props.fetching} type="primary" icon={rec.paymentVerified?"check":"upload"} >Payment Made</Button>
                        </Tooltip>
                    </Popconfirm>
                :
                    <Tooltip placement="left" title="Click to view payment proof"><a href={rec.pdf} target="_blank"><Icon type="file-pdf" /> View Proof</a></Tooltip>
        }];

        const { transactions, fetching, pageCount } = this.props;
        
        return (
            <Layout style={{margin: 10}}>
                <Content>
                    {transactions.length > 0?<Content style={{textAlign: 'right', marginTop: 20, marginBottom: 20}}><Pagination showQuickJumper defaultCurrent={this.state.currentPage} total={pageCount?pageCount:1} onChange={this.onPaginationChange} /></Content>:null}
                    <Table bordered style={{background: '#fff'}} pagination={false} loading={this.props.fetching} columns={columns} dataSource={transactions}/>
                </Content>
            </Layout>
        )
    }
}

const mapStateToProps = ({transactions}) => {

    let bookings = transactions.data?transactions.data.bookings:[], arr = [];

    bookings?bookings.forEach(booking => {
        arr.push(Object.assign({}, booking, 
            {eventDate: moment(booking.eventDate).format(DATE_FORMAT)},
            {key: booking.id},
            {cancellationCutOffDate: moment(booking.cancellationCutOffDate).format(DATE_FORMAT)},
            {paymentDueDate: {text: moment(booking.paymentDueDate).format(DATE_FORMAT), tooltip: moment(booking.paymentDueDate).endOf('day').fromNow()}},
            {paymentVerified: booking.paymentVerified}
        ))
    }):null

    return {
        transactions: arr,
        fetching: transactions.fetching,
        pageCount:  transactions.data?transactions.data.count:1,        
        pendingTransactions: transactions.data?transactions.data.pending:0,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTransactionDetails: (pageId) => dispatch(getTransactionDetails(pageId)),
        setTransactionProof: (pdf, pageId) => dispatch(setTransactionProof(pdf, pageId)),        
        setScreen: screen => dispatch(setCurrentScreen(screen))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);