import React, { Component } from 'react';

import {
  Navbar,
  Nav,
  NavItem
} from 'react-bootstrap';

import { Icon } from 'antd';

const { Header, Brand, Toggle, Collapse } = Navbar;

/**
 * Class representing Navigation Bar.
 * Uses on bootstrap navigation.
 */
class Navigation extends Component {

    render () {
        return (
            // Navigation bar
            <Navbar fluid collapseOnSelect fixedTop style={wrapperStyles}>

                {/* Navbar header with brand */}
                <Header>
                    <Brand><a style={{display: 'inline-block', marginLeft: 20}} href={`http://localhost:3000`}><img style={{width: 80, position: 'relative', top: -10}} src="https://res.cloudinary.com/dgcojyezg/image/upload/v1511450194/millionspaces/logo.svg" alt="logo"/></a></Brand>
                    <Toggle />
                </Header>

                {/* Collapsible content */}
                <Collapse>
                    <Nav pullRight>
                        <NavItem eventKey={1}></NavItem>
                        <NavItem eventKey={2}><Icon type="phone" /> 011 7 811 811</NavItem>
                        <NavItem eventKey={3} href="#">Logout</NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}

export default Navigation;

// --------------------- Styels ---------------------
const wrapperStyles = {
    background: '#fff',
    width: '100vw',
    right: 0,
    left: 'auto',
    transition: 'width 0.175s'
}
