import React from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Layout, Menu, Icon } from 'antd';

// Routing
import { Link } from 'react-router-dom';
import index from 'google-maps-react';

import logoFull from '../../../images/logo-full.png';
import logoLetter from '../../../images/logo-letter.png';

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

/**
 * Class Representing Collapsible main sidebar
 */
class MainAside extends React.Component {

    static propTypes = {
        collapsed: PropTypes.bool.isRequired,
        onCollapse: PropTypes.func.isRequired
    }

    render () {
        return (
            <Sider
                collapsible
                collapsed={this.props.collapsed}
                onCollapse={this.props.onCollapse}
                style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0, zIndex: 9 }}
            >
                <div>{this.props.collapsed?<img style={{marginLeft: 15, width: 50, marginTop: 30, marginBottom: 30}} src={logoLetter} />:<img style={{marginLeft: 25, width: 150, marginTop: 40, marginBottom: 40}}  src={logoFull} />}</div>

                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                
                    <Menu.Item key='home'>
                        <Link to="/">
                            <Icon type="home" />
                            <span>Home</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key='spaces'>
                        <Link to="/spaces">
                            <Icon type="shop" />
                            <span>Spaces</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="payment">
                        <Link to="/transactions">
                            <Icon type="swap" />
                            <span>Transactions</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="managedBookings">
                        <Link to="/managed-bookings">
                            <Icon type="wallet" />
                            <span>Manage Bookings</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="listOfGuests">
                        <Link to="/guests">
                            <Icon type="team" />
                            <span>Guests</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="tentetive">
                        <Link to="/tentetive-bookings">
                            <Icon type="credit-card" />
                            <span>Tentetive Bookings</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="spaceOfTheWeek">
                        <Link to="space-of-the-week">
                            <Icon type="heart-o" />
                            <span>Space of the week</span>
                        </Link>
                    </Menu.Item>

                </Menu>
            </Sider>
        )
    }
}

export default MainAside;


