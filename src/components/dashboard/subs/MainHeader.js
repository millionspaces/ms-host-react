// TODO: Validate Proptypes
// TODO: DOC
// TODO: move styles
import React from 'react';
import { Layout, Row, Col, Menu, Dropdown, Icon, Badge, Tag } from 'antd';
import { connect } from 'react-redux';


const { Header } = Layout;

/**
 * Stateless component Representing menu items
 * @param {Object} props 
 */
const OverlayMenu = ({handleLogout}) => <Menu>
    <Menu.Item key="1" ><span onClick={handleLogout}>Logout</span></Menu.Item>
</Menu>;


/**
 * Stateless component Representing Main Header.
 * @param {Object} props 
 */
const MainHeader = ({ collapsed, toggle, handleLogout, currentScreenName, spaceId, pendingTransactions, user }) => <Header style={{ border: '1px solid #e2e2e2', zIndex: 99, background: '#fff', padding: 0, position: 'fixed', width: '94vw', top: 0 }}>
    <Row>
        <Col span={12}>
            <Icon
                style={{display: 'inline-block'}}
                className="trigger"
                type={collapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={toggle}
            />
            <p style={{display: 'inline-block', marginRight: 20}}><span style={{fontWeight: 'bold'}}>You are in:</span> <Tag>{currentScreenName}</Tag></p>
            {currentScreenName === 'Edit Space'?<span style={{marginRight: 20}}><span style={{fontWeight: 'bold'}}>Space ID: </span>{spaceId}</span>:null}
        </Col>
        <Col span={12}>
            <div style={{textAlign: 'right', marginRight: 10}}>
                    <span style={{fontStyle: "italic", fontWeight: 'bold'}}>Howdy {user}!</span> <Dropdown overlay={<OverlayMenu handleLogout={handleLogout} />} trigger={['click']}>
                    <a className="ant-dropdown-link"><Icon type="USER" /><Icon type="down" /></a>
                </Dropdown>
            </div>
        </Col>
    </Row>
</Header>

export default connect(({screens, space, auth}) => {
    const { mainScreen } = screens;
    const { id } = space.data;
    return {
        currentScreenName: mainScreen,
        spaceId: id,
        user: auth.name?auth.name:'Admin'
    }
}, null) (MainHeader);
