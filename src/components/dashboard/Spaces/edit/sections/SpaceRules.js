import React from 'react';

//Ant design
import { Form, Button, Checkbox } from 'antd';

const FormItem = Form.Item;
const createFormField = Form.createFormField

/** 
 * Class representing Space Rules Component.
 */
class SpaceRules extends React.Component {

    /**
     * Compare two aplha values
     * @param {String} a
     * @param {String} b
     */
    compare = (a, b) => {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    }

    /**
     * Handles form submit.
     * @param {SyntheticEvent} e Describes the event triggered.
     */
    handleSubmit = (e) => {

        // Stop default action.
        e.preventDefault();

        // Validate all the form field when form is submitted
        this.props.form.validateFields((err, values) => {

            let arr = [];

            // Loop through options and construct an array of selected option values.            
            Object.keys(values).forEach(item => {
                if (values[item]) {
                    arr.push(item)
                }
            });

            // If validation is passed then update the space.
            if (!err) {

                let updates = undefined;
                updates = Object.assign({}, { rules: arr });
                this.props.updateSpace(updates);

            }
        });
    }

    render() {

        const { rules } = this.props;
        const { getFieldDecorator } = this.props.form;

        // This is to avoid data mutation beetween dispatches.
        let { spaceRules } = this.props;
        spaceRules = [...spaceRules];

        // Sort space rules in alphabetic order.
        spaceRules.sort(this.compare)

        return (
            <Form onSubmit={this.handleSubmit} onPaste={() => { window.confirm('Please think twice before copy pasting. You are going to make an innocent dev soul suffer for being a dev soul. There can be hidden characters which can cause bugs dont be lazy enter them manually. 😠😠😠😠') }}>

                {/* Space rules options */}
                <div>
                    {spaceRules && rules ? spaceRules.map(rule => <FormItem key={rule.id} style={{ marginRight: 40, display: 'inline-block', width: 200 }}>
                        {getFieldDecorator(`${rule.id}`, {
                            valuePropName: 'checked'
                        })(
                            <Checkbox>{rule.name}</Checkbox>
                        )}
                    </FormItem>) : null}
                </div>
                
                {/* Submit button */}     
                <div style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        )
    }
}


// Decorate space rules rules component by rcforms.
SpaceRules = Form.create({

    // Mapping component props to dynamic fields.
    mapPropsToFields(props) {


        const { rules } = props;

        let obj = {}

        // If rules available then loop throug them
        // and create a dynamic form field per each.
        if (rules) {
            rules.forEach(rule => {

                obj = Object.assign({}, obj, {
                    [rule]: createFormField({ value: true })
                });

            });
        }

        return obj;
    }
})(SpaceRules);

export default SpaceRules