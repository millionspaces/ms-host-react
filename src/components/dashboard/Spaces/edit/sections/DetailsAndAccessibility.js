// TODO: proptype validation.
import React from 'react';

//Ant design
import { Form, Button, Checkbox, Select, Radio, InputNumber } from 'antd';
import { hasErrors } from '../../../../../utils';

const FormItem = Form.Item,
      Option = Select.Option,
      createFormField = Form.createFormField,
      RadioButton = Radio.Button,
      RadioGroup = Radio.Group;

/**
 * Class representing Host Details Section of Basic Space Details and accessibility.
 */
class DetailsAndAccessibility extends React.Component {

    /**
     * Handles form submit.
     * @param {SyntheticEvent} e Describes the event triggered.
     */  
    handleSubmit = (e) => {

        // Prevent default action.
        e.preventDefault();

        let updates = {};

        // Validating values if no errors then update profile.
        this.props.form.validateFields((err, values) => {
            if (!err) {

                const { size, measurementUnit, spaceType } = values;

                let seatingArrangements = [],
                    eventType = [],
                    amenity = [],
                    extraAmenity = [],
                    seatingParticipantCounts = [];

                Object.keys(values).forEach(key => {
                    
                    let splittedKey = key.split('-');

                    if (splittedKey[1] === 'seatingCheckbox' && values[key]) {
                        seatingArrangements.push({
                            id: splittedKey[0],
                            participantCount: values[`${splittedKey[0]}-seatingInput`]
                        });

                        seatingParticipantCounts.push(Number(values[`${splittedKey[0]}-seatingInput`]));
                    }

                    if (splittedKey[1] === 'eventType' && values[key]) {
                        eventType.push(splittedKey[0]);
                    }
                    
                    if (splittedKey[1] === 'amenityType' && values[key] === 'cm') {
                        amenity.push(splittedKey[0])
                    }

                    if (splittedKey[1] === 'amenityType' && values[key] === 'ch') {
                        extraAmenity.push({
                            amenityId: splittedKey[0],
                            amenityUnit: values[`${splittedKey[0]}-amenityUnit`],
                            extraRate: values[`${splittedKey[0]}-amenityCharge`]
                        });
                    }
                });

                updates = Object.assign({}, { 
                    size,
                    measurementUnit: {id: measurementUnit},
                    seatingArrangements,
                    eventType,
                    spaceType: [{id: spaceType}],
                    amenity,
                    extraAmenity,
                    participantCount: Math.max(...seatingParticipantCounts)
                });

                this.props.updateSpace(updates);

            }
        });
    }

    /**
     * Validates space size.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    spaceSizeValidator = (rule, value, callback) => this.validateNumber( value, callback, 999999)

    /**
     * Validates seating.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    validateSeatingInput = (rule, value, callback) => this.validateNumber( value, callback, 99999);

    /**
     * Validates unit price.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    validateUnitPrice = (rule, value, callback) => this.validateNumber( value, callback, 99999);

    /**
     * Validates Number inputs.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    validateNumber = (value, callback, exceeded) => {
        if (!value) {
            callback ('This field is required.');
        } else if (!/^[1-9]\d*$/.test(value)) {
            callback ('Field can only contain positive digits.');
        } else if (value.length === 0) {
            callback ('This field is required.');
        } else if (value > exceeded) {
            callback (`Field value exceeded. Max ${exceeded}.`);
        } else {
            callback ();
        }
    }

    
    /**
     * Handles seating changes.
     * @param {SyntheticEvent} event Describes the event triggered.
     */  
    onSeatingCheckboxChange = event => {

        // TODO: Optimize performance
        if (!event.target.checked) {
            this.props.form.setFieldsValue({
                [`${event.target.id.split('-')[0]}-seatingInput`]: ''
            });
        }

    }

    render () {

        const { getFieldDecorator, getFieldValue, getFieldsError } = this.props.form;

        let { size, measurementUnit, spaceType, seatingArrangements, eventTypes, amenities, amenityUnits, spaceTypes } = this.props;

        return (
            <Form onSubmit={this.handleSubmit} >

                {/* Space size */}
                <FormItem label="Estimated Square footage / meterage" style={{width: 320, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('size', {
                        rules: [{ validator: this.spaceSizeValidator}],
                        initialValue: String(size)
                    })(
                        <InputNumber style={{ width: '100%' }} formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}/>
                    )}
                </FormItem >

                {/* Space size unit */}
                <FormItem label="Estimated space size unit." style={{width: 320, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('measurementUnit', {
                        initialValue: measurementUnit
                    })(
                        <Select>
                            <Option value={1}>sq ft.</Option>
                            <Option value={2}>sq m</Option>
                        </Select>
                    )}
                </FormItem >

                {/* Seating arrangements */}
                <div>
                    <h4 style={{fontWeight: 'bold'}}>Seating Arrangements</h4>
                    {
                        seatingArrangements.map(arrangement => {
                            return <FormItem key={arrangement.id} style={{width: 400, display: 'inline-block', marginRight: 20}}>
                                
                                {getFieldDecorator(`${arrangement.id}-seatingCheckbox`, {
                                    valuePropName: 'checked'
                                })(
                                    <Checkbox
                                        onChange={this.onSeatingCheckboxChange}
                                    >
                                        <img alt={arrangement.name} style={{width: 50}} src={arrangement.mobileIcon}/>
                                        <FormItem style={{display: 'inline-block'}}>{getFieldDecorator(`${arrangement.id}-seatingInput`, {
                                            rules: [{ validator: getFieldValue(`${arrangement.id}-seatingCheckbox`)?this.validateSeatingInput:null,
                                            required: getFieldValue(`${arrangement.id}-seatingCheckbox`)?true:false}],
                                        })(
                                            <InputNumber disabled={!getFieldValue(`${arrangement.id}-seatingCheckbox`)}  formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')} style={{ width: 150 }} />
                                        )}</FormItem>
                                    </Checkbox>
                                )}
                                {arrangement.name}
                                
                            </FormItem>
                        })
                    }
                </div>
                
                {/* Activities */}
                <div>
                    <h4 style={{fontWeight: 'bold'}}>Activities</h4>
                    {
                        eventTypes.map(eventType => {
                            return <FormItem key={eventType.id} style={{width: 400, display: 'inline-block', marginRight: 20}}>
                                {
                                    getFieldDecorator(`${eventType.id}-eventType`, {
                                        valuePropName: 'checked'
                                    })(
                                        <Checkbox>
                                            <img alt={eventType.name} style={{width: 30, marginRight: 10}} src={eventType.mobileIcon} />
                                            {eventType.name}
                                        </Checkbox>
                                    )
                                }
                            </FormItem>
                        })
                    }
                </div>
                
                {/* Space type */}
                <div>
                    <h4 style={{fontWeight: 'bold'}}>Space Type</h4>
                    <FormItem>
                        {
                            getFieldDecorator('spaceType', {
                                initialValue: spaceType && spaceType[0]?spaceType[0].id:''
                            })(
                                <RadioGroup>{ spaceTypes.map(spaceType => <Radio key={spaceType.id} value={spaceType.id}><img  alt={spaceType.name} style={{width: 20, marginRight: 10}} src={spaceType.mobileIcon}/>{spaceType.name}</Radio>)}</RadioGroup>
                            )
                        }
                    </FormItem>
                </div>

                {/* Space amenities */}
                <div>
                    <h4 style={{fontWeight: 'bold'}}>Amenities</h4>

                    {amenities.map(amenity => {
                        return <div key={amenity.id} style={{display: 'inline-block', width: '100%', marginBottom: 10}}><div style={{display: 'flex', alignItems: 'center', marginBottom: 10}}>
                            <img alt={amenity.name}  src={amenity.mobileIcon} style={{width: 20, marginRight: 15}}/>
                            <p style={{marginBottom: 0, marginRight: 10, width: 150}}>{amenity.name}</p>

                            <FormItem style={{marginBottom: 0}}>
                                {
                                    getFieldDecorator(`${amenity.id}-amenityType`)(
                                        <RadioGroup style={{marginRight: 20}}>
                                            <RadioButton value="na">NA</RadioButton>
                                            <RadioButton value="cm">Complimentary</RadioButton>
                                            <RadioButton value="ch">Chargeable</RadioButton>
                                        </RadioGroup>
                                    )
                                }
                            </FormItem>
                            <FormItem style={{marginBottom: 0}}>
                                <label>Unit Price (LKR): 
                                    {
                                        getFieldDecorator(`${amenity.id}-amenityCharge`, {
                                            rules: [{ 
                                                required: getFieldValue(`${amenity.id}-amenityType`)==='ch'?true:false, 
                                                validator:getFieldValue(`${amenity.id}-amenityType`)==='ch'? this.validateUnitPrice:null
                                            }],
                                        })(<InputNumber
                                            style={{marginLeft: 10, width: 100, marginRight: 20}} 
                                            disabled={getFieldValue(`${amenity.id}-amenityType`)!=='ch'?true:false}
                                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        />)
                                    }
                                </label>
                            </FormItem>
                            <FormItem style={{marginBottom: 0}}>
                                <label>Unit:
                                    {
                                        getFieldDecorator(`${amenity.id}-amenityUnit`)(
                                        <Select style={{ width: 120, marginLeft: 10 }} disabled={getFieldValue(`${amenity.id}-amenityType`)!=='ch'?true:false}>
                                            {amenityUnits.map(unit => <Option key={amenity.id} value={unit.id}>{unit.name}</Option>)}
                                        </Select>)
                                    }
                                </label>
                            </FormItem>
                           
                        </div></div>
                    })}
                    
                </div>

                {/* Update button */}
                <div style={{ textAlign: 'right' }}>
                    <Button disabled={hasErrors(getFieldsError())} type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        )
    }
}

DetailsAndAccessibility = Form.create({

    // Maping dynamic fields to props
    mapPropsToFields (props) {

        // Seating Arrangement dynamic fields
        let obj = {}
        
        let arrSeating = [];

        const { seatingArrangements, selectedSeatingArrangements, eventTypes, spaceEventTypes, chargeableAmenities, complimentaryAmenities, amenities } = props;
        
        // TODO: USE A COMMON FUNC
        const getVal = id => {

            let val = selectedSeatingArrangements.find(item => {
                return item.id === id
            });

            return val;
        }

        const getChargebaleAmenity = id => {

            let val = chargeableAmenities.find(item => {
                return item.amenityId === id
            });

            return val;
        }

        if (seatingArrangements && selectedSeatingArrangements) {
            
            selectedSeatingArrangements.forEach(item => {
                arrSeating.push(item.id);
            });
            
            seatingArrangements.forEach(item => {
                obj = Object.assign({}, obj, {
                    [`${item.id}-seatingCheckbox`]: createFormField({value: arrSeating.indexOf(item.id) !==-1?true:false}),
                    [`${item.id}-seatingInput`]: createFormField({value: getVal(item.id)?getVal(item.id)['participantCount']:''}),
                    [`${item.id}-seatingInput`]: createFormField({value: getVal(item.id)?getVal(item.id)['participantCount']:''})
                });
            });
        }
        
        if (eventTypes && spaceEventTypes) {
            eventTypes.forEach(item => {
                obj = Object.assign({}, obj, {
                    [`${item.id}-eventType`]: createFormField({value: spaceEventTypes.indexOf(item.id) !==-1?true:false})
                });
            });
        }

        if (complimentaryAmenities && chargeableAmenities &&  amenities) {

            let chargeableIds = [];

            chargeableAmenities.forEach(ch => {
                chargeableIds.push(ch.amenityId);
            });

            chargeableAmenities.forEach(ch => {
                chargeableIds.push(ch.amenityId);
            });

            amenities.forEach(item => {

                let type = '';

                if (complimentaryAmenities.indexOf(item.id) !==-1) {
                    type = 'cm';
                } else if (chargeableIds.indexOf(item.id) !==-1) {
                    type = 'ch';
                } else {
                    type = 'na';
                }

                obj = Object.assign({}, obj, {
                    [`${item.id}-amenityType`]: createFormField({value: type}),
                    [`${item.id}-amenityCharge`]: createFormField({value: type==='ch'?getChargebaleAmenity(item.id)['extraRate']: ''}),
                    [`${item.id}-amenityUnit`]: createFormField({value: type==='ch'?getChargebaleAmenity(item.id)['amenityUnit']: item.amenityUnit})
                });
                
            });

        }

        return obj;

    }
})(DetailsAndAccessibility);

export default DetailsAndAccessibility;