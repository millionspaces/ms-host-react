import React from 'react';

// Ant Design
import { Form, Input, Button } from 'antd';

// Custom components
import GMapContainer from '../../../../common/GMapContainer';
import { hasErrors } from '../../../../../utils';

const FormItem = Form.Item;
const { TextArea } = Input;

/**
 * Class representing Basic space info
 */
class BasicSpaceInfo extends React.Component {

    state={ latitude: this.props.latitude, longitude: this.props.longitude, addressLine1:undefined }

    /**
     * Handles form submit.
     * @param {SyntheticEvent} e Describes the event triggered.
     */  
    handleSubmit = (e) => {

        // Prevent default action.
        e.preventDefault();

        // Validate form fields, If no errors then update space.
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let updates = undefined;
                updates = Object.assign({}, values, { latitude: this.state.latitude, longitude: this.state.longitude });
                this.props.updateSpace(updates);
            }
        });
    }

    /**
     * Get location from GMapContainer and set it to component state.
     * @param {Object} location Describes geo location picked.
     */
    getLocation = location => {
        this.setState({
            latitude: location.latitude,
            longitude: location.longitude
        })
    }

    // ------------------ Validation methods ------------------

    /**
     * Validates Space name.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    spaceNameValidator = (rule, value, callback) => {

        if (value.length > 40) {
            callback('Space name can\'t exceed 40 characters.')
        } else if (value.length === 0) {
            callback ("This field is required");
        } else {
            callback ();
        }

    }

    /**
     * Validates organization name.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    organizationNameValidator = (rule, value, callback) => {

        if (value.length > 32) {
            callback('Organization name can\'t exceed 32 characters.')
        } else {
            callback ();
        }
        
    }

    /**
     * Validates space description.
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */
    spaceDescriptionValidator = (rule, value, callback) => {

        if (value.length > 300) {
            callback('Space description can\'t exceed 300 characters.')
        } else if (value.length === 0) {
            callback ('This field is required.');
        } else{
            callback ();
        }
        
    }

    render () {

        let { name, addressLine2, addressLine1, latitude, longitude, description } = this.props;
        const { getFieldDecorator, getFieldValue, getFieldsError } = this.props.form;

        return (
            <Form onSubmit={this.handleSubmit}>

                {/* Space name */}
                <FormItem label="Space Name" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('name', {
                        rules: [{validator: this.spaceNameValidator}],
                        initialValue: name
                    })(
                        <Input placeholder="Space Name: Eg. Auxenta Board Room (max length 40 Characters.)" />
                    )}
                </FormItem>

                {/* Organization name */}
                <FormItem label="Organization name (Optional)" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('addressLine2', {
                        rules: [{ validator: this.organizationNameValidator }],
                        initialValue: addressLine2
                    })(
                        <Input placeholder="Organization name." />
                    )}
                </FormItem>

                {/* Address */}
                <FormItem label="Address" style={{width: 300, display: 'inline-block', marginRight: 20}}>
                    {getFieldDecorator('addressLine1', {
                        rules: [{ required: true, message: 'Please Enter Address' }],
                        initialValue: this.state.addressLine1?this.state.addressLine1:addressLine1
                    })(
                        <Input placeholder="Address" />
                    )}
                </FormItem>

                {/* Google map */}
                <FormItem>
                    <GMapContainer
                        latitude={latitude}
                        longitude={longitude}
                        addressLine1={addressLine1}
                        getLocation={this.getLocation}
                    />
                </FormItem>

                {/* Space description */}
                <FormItem label="Space Description" style={{marginTop: 100}}>
                    {getFieldDecorator('description', {
                        rules: [this.spaceDescriptionValidator],
                        initialValue: description
                    })(
                        <TextArea placeholder="Space Description" style={{minHeight: 100}} />
                        
                    )}
                    <span style={{position: 'absolute', right: 10, bottom: -5, display: 'inline-block'}}>{300 - getFieldValue('description').length}</span>
                </FormItem>
                
                {/* Update button */}
                <div style={{ textAlign: 'right' }}>
                    <Button disabled={hasErrors(getFieldsError())} type="primary" htmlType="submit" className="login-form-button">Update</Button>
                </div>

            </Form>
        )
    }
}

BasicSpaceInfo = Form.create()(BasicSpaceInfo);
export default BasicSpaceInfo;