import React from 'react';

export const renderBlockBasedSpaceOnlyHeader = () => <div style={{display: 'flex'}} className="per-hour-table-header">
    <div>Day</div><div>From</div><div>To</div><div>Block Price (LKR)</div><div></div>
</div>

export const renderBlockBasedPerGuestHeader = () => <div style={{display: 'flex'}} className="per-hour-table-header">
    <div>Day</div><div>From</div><div>To</div><div>Min. Price per guest (LKR)</div><div>Menu</div><div>Min Block Price (LKR)</div><div></div>
</div>