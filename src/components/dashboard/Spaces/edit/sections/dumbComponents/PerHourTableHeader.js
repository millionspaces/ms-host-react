import React from 'react';


const PerHourTableHeader = () => <div style={{display: 'flex'}} className="per-hour-table-header">
    <div>Availability</div><div>Day</div><div>From</div><div>To</div><div>Charge per hour</div>
</div>

export default PerHourTableHeader;