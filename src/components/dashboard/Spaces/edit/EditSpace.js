// TODO: proptype validataion
// TODO: Implement Error Handling
import React from 'react';

// Ant design
import { Layout, Spin, notification } from 'antd';
import { Collapse } from 'antd';

// Sections
import HostDetails from './sections/HostDetails';
import BasicSpaceInfo from './sections/BasicSpaceInfo';
import DetailsAndAccessibility from './sections/DetailsAndAccessibility';
import PricingAvailability from './sections/PricingAvailability';
import SpaceRules from './sections/SpaceRules';
import CancellationPolicy from './sections/CancellationPolicy';
import BankDetails from './sections/BankDetails';
import SpaceImages from './sections/SpaceImages';
import HostLogo from './sections/HostLogo';
import MenuFiles from './sections/MenuFiles';


// Redux
import { connect } from 'react-redux';
import { getSpaceDetailsById, updateSpaceActionCreator } from '../../../../redux/actions/spacesActions';
import { setSpaceImagesDetails, getImgDetailsAndRemoveImageFromCloudinery } from '../../../../redux/actions/spaceImagesActions';

import { 
    getCancellationPolicies,
    getSpaceRules,
    getAllSeatingArrangements,
    getAllActivities,
    getAllAmenities,
    getAllAmenityUnits,
    getSpaceTypes,
    getBlockChargeTypes
} from '../../../../redux/actions/commonActions';
import { setCurrentScreen } from '../../../../redux/actions/screensActions';

const Panel = Collapse.Panel;

const panelStyles = {
    background: '#fff',
    borderRadius: 3,
    marginBottom: 21,
    border: 0,
    overflow: 'hidden'
}

/**
 * Class representing Edit Space Component
 */
class EditSpace extends React.Component {

    state = {
        fetching: false,
        data: []
    }

    // ------------------------- Life cycle methods -------------------------

    componentDidMount () {
        this.props.getSpaceDetailsById(this.props.spaceId);
        this.props.getAllSeatingArrangements();
        this.props.getAllActivities();
        this.props.getAllAmenities();
        this.props.getAmenityUnits();
        this.props.getSpaceTypes();
        this.props.getBlockChargeTypes();
        this.props.getSpaceRules();
        this.props.getCancellationPolicies();
        this.props.setScreen('Edit Space');
    }
    

    componentWillReceiveProps (nextProps) {

        let { fetching, error, id, hostLogo, updateSuccess } = nextProps;

        this.setState({ fetching, error });

        if (id) {
            this.setState ({ id, hostLogo });
        }

        if (updateSuccess) {
            this.openNotificationWithIcon('success')
        }

    }


    // ------------------------- Custom methods -------------------------

    openNotificationWithIcon = (type) => {
        notification[type]({
            message: 'Changes successfully saved'
        });
    };

    /**
     * Hanlde space update button hits.
     */
    updateSpace = changes => {

        let space = Object.assign ({}, this.props.data, changes);
        this.props.updateSpace(space);

    }


    render () {

        let {fetching, id, hostLogo} = this.state,
            { 
                contactPersonName,
                contactPersonName2,
                mobileNumber, 
                mobileNumber2,
                companyPhone, 
                name, 
                addressLine2, 
                addressLine1,
                latitude,
                longitude,
                description,
                size,
                measurementUnit,
                participantCount,
                minParticipantCount,
                selectedSeatingArrangements,
                spaceEventTypes,
                eventTypes,
                complimentaryAmenities,
                chargeableAmenities,
                amenities,
                amenityUnits,
                spaceTypes,
                spaceType,
                availabilityMethod,
                availability,
                seatingArrangements,
                bufferTime,
                noticePeriod,
                calendarEnd,
                blockChargeTypes,
                blockChargeType,
                reimbursableOption,
                menuFiles,
                spaceRules,
                rules,
                cancellationPolicies,
                accountHolderName,
                accountNumber,
                bank,
                bankBranch,
                cancellationPolicy,
                thumbnailImage,
                images,
                pricingFetching
            } = this.props;


        return (

            <Spin spinning={fetching}>
                <Layout style={{background: '#fff', margin: 20}}>
                    <Collapse bordered={false}  style={{background: '#f0f2f5'}}>
                        
                        <Panel key={0} header={<h3 style={{marginBottom: 0}}>Host Logo</h3>} style={panelStyles}>
                            <HostLogo
                                spaceId={id}
                                hostLogo={hostLogo}
                                updateSpace={this.updateSpace}
                            />
                        </Panel>

                        <Panel key={1} header={<h3 style={{marginBottom: 0}}>Host details</h3>} style={panelStyles}>
                            <HostDetails
                                spaceId={id}
                                hostLogo={hostLogo}
                                contactPersonName={contactPersonName}
                                contactPersonName2={contactPersonName2}
                                mobileNumber={mobileNumber}
                                mobileNumber2={mobileNumber2}
                                companyPhone={companyPhone}
                                updateSpace={this.updateSpace}
                            />
                        </Panel>

                        <Panel key={2} header={<h3 style={{marginBottom: 0}}>Basic Space Info</h3>} style={panelStyles}>
                            <BasicSpaceInfo
                                spaceId={id}
                                updateSpace={this.updateSpace}
                                name={name}
                                addressLine2={addressLine2}
                                addressLine1={addressLine1}
                                latitude={latitude}
                                longitude={longitude}
                                description={description}
                            />
                        </Panel>

                        <Panel key={8} header={<h3 style={{marginBottom: 0}}>Space Images</h3>} style={panelStyles}>
                            <SpaceImages
                                thumbnailImage={thumbnailImage}
                                updateSpace={this.updateSpace}
                                images={images}
                            />
                        </Panel>

                        <Panel key={3} header={<h3 style={{marginBottom: 0}}>Space Details and Accessibility</h3>} style={panelStyles}>
                            <DetailsAndAccessibility
                                spaceId={id}
                                updateSpace={this.updateSpace}
                                size={size}
                                measurementUnit={measurementUnit}
                                participantCount={participantCount}
                                seatingArrangements={seatingArrangements}
                                selectedSeatingArrangements={selectedSeatingArrangements}
                                eventTypes={eventTypes}
                                spaceEventTypes={spaceEventTypes}
                                complimentaryAmenities={complimentaryAmenities}
                                chargeableAmenities={chargeableAmenities}
                                amenities={amenities}
                                amenityUnits={amenityUnits}
                                spaceTypes={spaceTypes}
                                spaceType={spaceType}
                            />
                        </Panel>

                        <Panel key={'menu-files'} header={<h3 style={{marginBottom: 0}}>Menu Files</h3>} style={panelStyles}>
                            <MenuFiles
                               menuFiles={menuFiles?menuFiles:[]}
                               updateSpace={this.updateSpace}
                               saveSpaceImages={this.props.saveSpaceImages}
                               removeImageFromCloud={this.props.removeImageFromCloud}
                            />
                        </Panel>

                        <Panel key={4} header={<h3 style={{marginBottom: 0}}>Pricing and availability</h3>} style={panelStyles}>
                            <PricingAvailability
                                noticePeriod={noticePeriod}
                                calendarEnd={calendarEnd}
                                updateSpace={this.updateSpace}
                                availabilityMethod={availabilityMethod}
                                availability={pricingFetching?availability:availability}
                                blockChargeTypes={blockChargeTypes}
                                blockChargeType={blockChargeType}
                                reimbursableOption={reimbursableOption}
                                menuFiles={menuFiles?menuFiles:[]}
                                bufferTime={bufferTime}
                                participantCount={participantCount}
                                minParticipantCount={minParticipantCount}
                                saveSpaceImages={this.props.saveSpaceImages}
                                removeImageFromCloud={this.props.removeImageFromCloud}
                                pricingFetching={pricingFetching}
                            />
                        </Panel>

                        <Panel key={5} header={<h3 style={{marginBottom: 0}}>Space Rules</h3>} style={panelStyles}>
                            <SpaceRules
                                spaceRules={spaceRules}
                                rules={rules}
                                updateSpace={this.updateSpace}
                            />
                        </Panel>

                        <Panel key={6} header={<h3 style={{marginBottom: 0}}>Cancellation Policy</h3>} style={panelStyles}>
                            <CancellationPolicy
                                cancellationPolicies={cancellationPolicies}
                                cancellationPolicy={cancellationPolicy}
                                updateSpace={this.updateSpace}
                            />
                        </Panel>

                        <Panel key={7} header={<h3 style={{marginBottom: 0}}>Bank Details</h3>} style={panelStyles}>
                            <BankDetails
                                accountHolderName={accountHolderName}
                                accountNumber={accountNumber}
                                bank={bank}
                                bankBranch={bankBranch}
                                updateSpace={this.updateSpace}
                            />
                        </Panel>

                        
                        
                    </Collapse>
                </Layout>
            </Spin>
        )
    }
}

const mapStateToProps = ({space, seatingArrangements, activities, amenities, amenityUnits, spaceTypes, blockChargeTypes, spaceRules, cancellationPolicies }) => {

    const { data, error, updateSuccess } = space;

    const { 
        id,
        hostLogo,
        contactPersonName,
        contactPersonName2,
        mobileNumber,
        mobileNumber2,
        companyPhone,
        name,
        addressLine2,
        addressLine1,
        latitude,
        longitude,
        description,
        size,
        measurementUnit,
        participantCount,
        minParticipantCount,
        eventType,
        amenity,
        extraAmenity,
        spaceType,
        availabilityMethod,
        availability,
        noticePeriod,
        bufferTime,
        calendarEnd,
        blockChargeType,
        menuFiles,
        reimbursableOption,
        rules,
        cancellationPolicy,
        accountHolderName,
        accountNumber,
        bank,
        bankBranch,
        thumbnailImage,
        images
    } = data;

    return {

        id,
        hostLogo,
        contactPersonName,
        contactPersonName2,
        mobileNumber,
        mobileNumber2,
        companyPhone,
        data,
        updateSuccess,
        name,
        addressLine2,
        addressLine1:addressLine1?addressLine1:'',
        latitude,
        longitude,
        description,
        size,
        measurementUnit: measurementUnit?measurementUnit.id:undefined,
        participantCount,
        minParticipantCount,
        selectedSeatingArrangements: data?data.seatingArrangements:undefined,
        seatingArrangements: seatingArrangements?seatingArrangements.data:undefined,
        eventTypes: activities?activities.data:false,
        spaceEventTypes: eventType,
        amenities: amenities.data,
        complimentaryAmenities: amenity,
        chargeableAmenities: extraAmenity,
        amenityUnits: amenityUnits.data,
        spaceType,
        availabilityMethod,
        availability,
        spaceTypes: spaceTypes?spaceTypes.data:undefined,
        noticePeriod,
        bufferTime,
        calendarEnd,
        blockChargeTypes: blockChargeTypes?blockChargeTypes.data:[],
        blockChargeType,
        reimbursableOption,
        menuFiles,
        spaceRules: spaceRules?spaceRules.data:undefined,
        rules,
        cancellationPolicy,
        accountHolderName,
        accountNumber,
        bank,
        bankBranch,
        cancellationPolicies: cancellationPolicies?cancellationPolicies.data:undefined,
        thumbnailImage,
        images,
        pricingFetching: space?space.fetching:false,
        fetching: space?space.fetching:false 
                    && seatingArrangements?seatingArrangements.fetching:false
                    && activities?activities.fetching:false
                    && amenities?amenities.fetching:false
                    && amenityUnits?amenityUnits.fetching:false
                    && spaceTypes?spaceTypes.fetching:false
                    && blockChargeTypes?blockChargeTypes.fetching:false
                    && spaceRules?spaceRules.fetching:false
                    && cancellationPolicies?cancellationPolicies.fetching:false,

        error
    }
};

const mapDispatchToProps = dispatch => {

    return {
        getSpaceDetailsById: id => dispatch (getSpaceDetailsById(id)),
        updateSpace: space => dispatch(updateSpaceActionCreator(space)),
        getAllSeatingArrangements: () => dispatch(getAllSeatingArrangements()),
        getAllActivities: () => dispatch(getAllActivities()),
        getAllAmenities: () => dispatch(getAllAmenities()),
        getAmenityUnits: () => dispatch(getAllAmenityUnits()),
        getSpaceTypes:  () => dispatch(getSpaceTypes()),
        getBlockChargeTypes: () => dispatch(getBlockChargeTypes()),
        getSpaceRules: () => dispatch(getSpaceRules()),
        getCancellationPolicies: () => dispatch(getCancellationPolicies()),
        setScreen: screen => dispatch(setCurrentScreen(screen)),
        saveSpaceImages: imageData => dispatch(setSpaceImagesDetails(imageData)),
        removeImageFromCloud: url => dispatch(getImgDetailsAndRemoveImageFromCloudinery(url))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (EditSpace);