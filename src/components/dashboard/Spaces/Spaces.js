import React from 'react';
import { connect } from 'react-redux';

import { getAllSpacesOfUser } from "../../../redux/actions/spacesActions";
// Ant Design
import { Layout, Table, Switch, Icon } from 'antd';
// React Router
import { Link } from 'react-router-dom';

const { Content } = Layout;

class Spaces extends React.Component {

    state = {
        fetching: false,
        spaces: [],
        currentPage: 1,
        totalPages: 0,
        spaceToggleFetching: false,
        prevToggledId: undefined
    }

    /**
     * Triggers on pagination status switch changes.
     * @param {Number} id Id of space.
     * @param {Boolean} checked Switch state.
     */
    onSpaceStatusChange = id => checked => {
        this.props.toggleSpaceStatus(id);
        this.setState({
            prevToggledId: id
        });
    }

    componentDidMount () {
        this.props.getSpaces(104)
    }

    componentWillReceiveProps(nextProps) {

        let { fetching, spaces, totalPages, spaceToggleFetching, spaceToggledId } = nextProps;

        this.setState({
            fetching
        });

        if (spaces) {

            let temp = [];

            spaces.forEach(item => {
                temp.push({
                    key: item.id,
                    id: item.id,
                    name: item.name,
                    status: item.approved
                });
            });

            this.setState({
                spaces: [...temp]
            });
        }

        if (totalPages) {
            this.setState({
                totalPages
            });
        }

        // If a space state is changed then refetch the api
        // TODO: Tuneup performance
        if (spaceToggledId) {
            this.setState({
                spaceToggleFetching
            });
            if (this.state.prevToggledId === spaceToggledId) {
                this.props.getSpacesSet(this.state.currentPage);
                this.setState({ prevToggledId: 0 })
            }
        }

    }

    render () {

        let { fetching, spaces } = this.state;

        const columns = [{
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            fixed: 'left',
            width: 100
        }, {
            title: 'Space Name',
            dataIndex: 'name',
            key: 'name'
        },{
            title: 'Status',
            key: 'status',
            fixed: 'right',
            width: 80,
            render: (text, record) => (
                <Switch checked={Boolean(record.status)} onChange={this.onSpaceStatusChange(record.id)} />
            )
        }, {
            title: '',
            dataIndex: 'edit',
            fixed: 'right',
            width: 50,
            render: (text, record) => (
                <Link to={`/spaces/${record.id}`}><Icon type="edit" /></Link>
            )
        }];

        return (
            <Layout style={{ margin: 10 }}>
                
                <Content style={{ background: '#fff' }}>
                    <Table scroll={{ x: 1800 }} bordered pagination={false} loading={fetching} columns={columns} dataSource={spaces} />
                </Content>
            
            </Layout>
        )
    }
}

export default connect(({spaces}) => {

    const { data, fetching } = spaces;

    return {
        spaces: data ? data : undefined,
        fetching
    }
}, dispatch => {
    return {
        getSpaces: user => dispatch(getAllSpacesOfUser(user))
    }
}) (Spaces);