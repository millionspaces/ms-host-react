import React from 'react';
import { Layout, Table, Pagination, Button, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import moment from 'moment';

import { DATE_FORMAT, TIME_STRING_FORMAT } from '../../../settings';
import { getTentetiveBookings, confirmTentetiveBookingActionCreator } from '../../../redux/actions/paymentActionsCreator';
import { setCurrentScreen } from '../../../redux/actions/screensActions';

const Content = Layout.Content;

/**
 * Class representing Tentetive Bookings component.
 */
class TentetiveBookings extends React.Component {

    state = {
        currentPage: 1
    }

    // -------------------------- Custom Methods -------------------------------

    /**
     * Triggeres when pagination numbers are hit.
     * @param {String} pageNumber Number of page.
     */
    onPaginationChange = pageNumber => {
        // Deduct 1 from current page count as antd pagination starts with count 1 and api page count is zero based.
        this.props.getTentetiveBookings(pageNumber - 1);

        this.setState({
            currentPage: pageNumber
        });
    }

    // -------------------------- Life cycle methods ---------------------------

    componentDidMount() {

        // Once the component is mount then fetch all the tentetive bookings.
        // Deduct 1 from current page count as antd pagination starts with count 1 and api page count is zero based.
        this.props.getTentetiveBookings(this.state.currentPage - 1);

        this.props.setScreen('Tentetive Bookings')
    }

    render() {

        const { tentetiveBookings, totalPages } = this.props;
        const dataSource = [];

        // Tentetive bookings table columns.
        const columns = [
            {
                title: 'Ref',
                dataIndex: 'orderId',
                key: 'orderId',
                width: 100
            },
            {
                title: 'Space',
                dataIndex: 'space',
                key: 'space'
            },
            {
                title: 'Organization',
                dataIndex: 'organization',
                key: 'organization'
            },
            {
                title: 'Event Date',
                dataIndex: 'eventDate',
                key: 'eventDate'
            }, {
                title: 'Time',
                dataIndex: 'dates',
                key: 'dates',
                render: (rec = []) =><div>
                    {rec.map(element => {
                        return <div key={`${element.fromDate}-${element.toDate}`}>
                            <span style={{ display: 'inline-block', width: 100 }}>{moment(Number(element.fromDate)).format(`${TIME_STRING_FORMAT}`)}</span> - <span style={{ display: 'inline-block', width: 100, marginLeft: 20 }}>{moment(Number(element.toDate)).format(`${TIME_STRING_FORMAT}`)}</span></div>}
                    )}
                </div>
            }, {
                title: 'Total',
                dataIndex: 'total',
                key: 'total'
            }, {
                title: '',
                dataIndex: 'confirm',
                key: 'confirm',
                render: (text, rec) => 
                <Popconfirm title="Are you sure you want to confirm this transaction?" onConfirm={() => {this.props.confirmTentetiveBooking(rec.key)}} okText="Yes" cancelText="No">
                    <Button size="small" type="primary" >Verify Payment</Button>
                </Popconfirm>
            }
        ];
    

        // Tentetive bookings data source construction.
        tentetiveBookings ? tentetiveBookings.forEach(booking => {
            dataSource.push({
                key: booking.id,
                orderId: booking.orderId,
                space: booking.name,
                organization: booking.organizationName,
                eventDate: moment(booking.eventDate).format(DATE_FORMAT),
                dates: booking.dates,
                total: booking.total
            })
        }) : null;

        return (
            <Layout style={{ padding: 10 }}>
                <Content>
                
                    {dataSource.length > 0 ? <Content style={{textAlign: 'right', marginTop: 20, marginBottom: 20 }}><span style={{ marginRight: 10, display: 'inline-block', verticalAlign: 'middle'}}>Showing {dataSource.length} of {this.props.totalPages} Bookings  </span><Pagination style={{display: 'inline-block', verticalAlign: 'middle'}} showQuickJumper defaultCurrent={this.state.currentPage} total={totalPages} onChange={this.onPaginationChange} /></Content> : null}
                    <Table loading={this.props.fetching} bordered pagination={false} style={{ background: '#fff' }} dataSource={dataSource} columns={columns} />
                    
                </Content>
            </Layout>
        )
    }
}

const mapStateToProps = ({ tentetiveBookings }) => {
    return {
        tentetiveBookings: tentetiveBookings ? tentetiveBookings.data.bookings : [],
        totalPages: tentetiveBookings ? tentetiveBookings.data.count : undefined,
        fetching: tentetiveBookings.fetching
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getTentetiveBookings: pageNumber => dispatch(getTentetiveBookings(pageNumber)),
        confirmTentetiveBooking: bookingId => dispatch(confirmTentetiveBookingActionCreator(bookingId)),
        setScreen: screen => dispatch(setCurrentScreen(screen))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TentetiveBookings);