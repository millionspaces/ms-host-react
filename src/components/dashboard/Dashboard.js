// TODO: Validate Proptypes
// TODO: DOC
// TODO: move styles
import React from 'react';

// Ant Design
import { Layout } from 'antd';

// Main Components
import Spaces from './Spaces/Spaces';
import EditSpace from './Spaces/edit/EditSpace';
import Navigation from './subs/Navigation';

// React Router
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Redux
import { userDeviceLogoutActionCreator } from '../../redux/actions/authenticationActions';
import { connect } from 'react-redux';


/**
 * Class representing Dashboard component
 */
class Dashboard extends React.Component {

    state = {
        collapsed: true
    }

    // -------------- Custom methods ---------------

    /**
     * Handles Sider Collapse state
     * @param {Boolean} collapsed
     */
    onCollapse = collapsed => {
        this.setState({ collapsed });
    }

    /**
     * Toggles Sider collapse state with collapsed button hits
     */
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    /**
     * Handle Logout
     */
    handleLogout = () => {
        this.props.logoutUserDevice();
    }

    render () {
        return (

            <Router>
                <Layout style={{marginTop: 50}}>
                    <Navigation />
                    <Route
                        exact
                        path="/"
                        render={() => <Spaces />}
                    />

                    <Route
                        exact
                        path="/spaces/"
                        render={() => <Spaces />}
                    />

                    <Route
                        exact
                        path="/spaces/:id"
                        render={props => <EditSpace spaceId={props.match.params.id}/>}
                    />

                </Layout>
            </Router>

        )
    }
}
export default connect(null, dispatch => {
    return {
        logoutUserDevice: () => dispatch(userDeviceLogoutActionCreator())
    }
}) (Dashboard);
