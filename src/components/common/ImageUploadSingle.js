// TODO: make it reusable.
// TODO: variablize for build

import React from 'react';
import { Icon, Spin, message, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import { getSpaceImageData, removeImageFromCloudinery } from '../../redux/actions/spaceImagesActions';
import { IMAGE_CLOUD_NAME, IMAGE_CLOUD_PRESET } from '../../settings';


/**
 * Class representing Image upload component
 */
class ImageUploadSingle extends React.Component {

    state = {
        fetching: false,
        imageUrl: this.props.defaultImg,
        deleteToken: undefined
    }

    // ----------------------------- Custome methods -----------------------------

    /**
     * Opens Cloudinary widget.
     */
    openWidget = () => {
        
        window.cloudinary.openUploadWidget({
            cloud_name: IMAGE_CLOUD_NAME,
            upload_preset: IMAGE_CLOUD_PRESET,
            theme: 'white',
            folder: 'millionspaces',
            client_allowed_formats: ["png", "jpeg"],
        },  (error, result) => {

            if (!error) {

                // If image successfully uploaded to cloudinery then set image url in container component state for form submit.
                this.props.onImageSaveSuccess(result[0].public_id.split('/')[1]); 

                // Showing image internally.
                this.setState({ imageUrl: result[0].public_id.split('/')[1], deleteToken: result[0].delete_token });

                // Set Image upload data from cloudinery to be saved with form submit
                // This is for later image manipulation. Eg: Delete image.
                this.props.setImageUploadData ({
                    imageDetails: [{  
                        url: result[0].public_id.split('/')[1],
                        publicId: result[0].public_id,
                        deleteToken: result[0].delete_token,
                        etag: result[0].etag,
                        signature: result[0].signature,
                        secureUrl: result[0].secure_url
                    }]
                });

                this.props.updateSpace({ hostLogo: result[0].public_id.split('/')[1] })
                
            }  else {
                message.error(error.message)
            }   

        });

    }

    /**
     * Remove Image from component state
     */
    removeImage = () => {        

        this.setState ({
            imageUrl: undefined
        });

        this.props.updateSpace({ hostLogo: '' });
        this.props.removeImageFromCouldinery (this.state.deleteToken)
        
    }

    // ----------------------------- Life Cycle Methods -----------------------------

    componentDidMount () {
        // Get host logo details.
        this.props.getHostLogo(this.state.imageUrl)
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.deleteToken) {
            this.setState ({
                deleteToken: nextProps.deleteToken
            });
        }

    }
    
    render () {
        
        const { fetching, imageUrl } = this.state;

        return (
            <span style={wrapperStyles}>

                {/* Note and pup confirm */}
                <p style={{width: '100%', display: 'block'}}><span style={{color: 'tomato'}}>* </span>Please note once an image is uploaded it is immediately updated on www.millionspaces.com</p>
                {imageUrl?<Popconfirm title="Are you sure you want to delete host logo?" onConfirm={() => { this.props.removeImage();  this.removeImage(); }}><Icon type="delete" style={iconStyles} /></Popconfirm>:null}

                {/* Upload button. */}
                <div onClick={this.openWidget} style={imageThumbStyles}>
                    <Spin spinning={fetching} >{imageUrl?<img alt="User thumbnail" src={`http://res.cloudinary.com/${IMAGE_CLOUD_NAME}/image/upload/millionspaces/${imageUrl}.jpg`} style={{width: 90, height: 90}}/>:<span><Icon type="user" style={{fontSize: 32}}/><Icon type="plus" style={{fontSize: 16, fontWeight: 'bold'}}/></span>}</Spin>
                </div>

            </span>
        );
    }
}

export default connect (({hostLogoData}) => {

    const { data } = hostLogoData;

    return {
        deleteToken: data?data.deleteToken:undefined,
        public_id: data?data.publicId:undefined
    }

}, dispatch => {
    return {
        getHostLogo: url => dispatch (getSpaceImageData(url)),
        removeImageFromCouldinery: (token, public_id) => dispatch(removeImageFromCloudinery(token, public_id))
    };
}) (ImageUploadSingle);


// Styling 

// TODO: Move to a seperate file.
const wrapperStyles = {alignItems: 'center', position: 'relative', display: 'inline-block'};
const iconStyles = {position: 'absolute', left: 78, bottom: 2, color: 'red', zIndex: 9999, cursor: 'pointer', fontSize: 22};
const imageThumbStyles = {cursor: "pointer", display: 'flex', justifyContent: 'center', alignItems: 'center', width: 100, height: 100, border: '1px dashed #e2e2e2'};