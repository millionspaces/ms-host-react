import React, { Component } from 'react';

// Ant Design
import { Layout } from 'antd';
import marked from "marked";

import { setCurrentScreen } from '../../redux/actions/screensActions';
import { connect } from 'react-redux';

class ReleaseNotes extends Component {

    state = {}

    componentDidMount () {
        this.props.setCurrentScreen ('Release');
    }

    componentWillMount() {

		const releaseNotes = require("../../release-notes.md");

		fetch(releaseNotes)
			.then(response => {
				return response.text()
			})
			.then(text => {
				this.setState({
					markdown: marked(text)
				});
			}).catch (err => {
				console.log(err)
			});
    }
    

    render() { 

        const { markdown } = this.state;
        
        return (
            <Layout style={{ minHeight: '100vh', minWidth: '97vw', paddingBottom: 45 }} >
                <article style={{background: '#fff', padding: 20}} dangerouslySetInnerHTML={{__html: markdown?markdown:'Nothing yet, will be updated soon'}}></article>
            </Layout>
        )
    }
}
 
export default connect (null, dispatch => ({setCurrentScreen: screen => dispatch(setCurrentScreen('Releases'))})) (ReleaseNotes);