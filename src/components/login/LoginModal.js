import React from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Layout, Card, Form, Icon, Input, Button, Spin } from 'antd';

// Redux
import { userDeviceLoginActionCreator } from '../../redux/actions/authenticationActions';
import { connect } from 'react-redux';

const Content = Layout;
const FormItem = Form.Item;
/**
 * Class representing Login Modal Component
 */
class LoginModal extends React.Component {

    static = {
        loggedIn: PropTypes.bool.isRequired
    }

    /**
     * Handles Form Submition
     * @param {SyntheticEvent} e Describes Event triggered.
     */
    handleLogin = e => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.props.loginUserDevice(values)
			}
		});
    }

    componentDidMount () {
        this.props.form.validateFields();
    }
    
    hasErrors = fieldsError => Object.keys(fieldsError).some(field => fieldsError[field]);
    
    render () {
        
        const { fetching } = this.props;
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        
        const userNameError = isFieldTouched('userName') && getFieldError('userName');
        const passwordError = isFieldTouched('password') && getFieldError('password');

        return (
            <Layout style={{width: '100vw', height: '100vh'}}>
                <Card style={{ width: 300, margin: 'auto' }}>
                    <Content style={{background: '#fff'}}>
                        <Spin spinning={fetching} >

                            <Form style={{background: '#fff'}} onSubmit={this.handleLogin} className="login-form">
                                
                                <div style={{textAlign: 'center', marginBottom: 20}}>
                                    <img alt="" style={{display: 'inline-block', width: 100}} src="https://res.cloudinary.com/dgcojyezg/image/upload/v1511499131/millionspaces/logo.svg"/>
                                </div>
                                <FormItem
                                    validateStatus={userNameError ? 'error' : ''}
                                    help={userNameError || ''}
                                >
                                    {getFieldDecorator('userName', {
                                        rules: [{
                                            required: true,
                                            message: 'Please enter your username'
                                        }]
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                    )}
                                </FormItem>

                                <FormItem
                                    validateStatus={passwordError ? 'error' : ''}
                                    help={passwordError || ''}
                                >
                                    {getFieldDecorator('password', {
                                        rules: [{required: true, message:"Please enter your password"}]
                                    })(
                                        <Input type="password" prefix={<Icon type="lock" style={{color:"rgba(0, 0, 0, 0,25)"}}/>} placeholder="password"/>
                                    )}
                                </FormItem>

                                <FormItem style={{textAlign: 'center'}}>
                                    <Button disabled={this.hasErrors(getFieldsError())} type="primary" htmlType="submit" className="login-form-button">
                                        Log in
                                    </Button>
                                </FormItem>

                            </Form>

                        </Spin>
                    </Content>
                </Card>
            </Layout>
        );
    }
}

LoginModal = Form.create()(LoginModal);

export default connect(null, dispatch => ({
    loginUserDevice: user=>dispatch(userDeviceLoginActionCreator(user))
})) (LoginModal);