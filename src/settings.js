// Environment
export const QA = 'https://qapi.millionspaces.com/api/';
export const LIVE = 'https://api.millionspaces.com/api/';

export let api = process.env.NODE_ENV ==='production' && process.env.REACT_APP_MS_ENV === 'live'?LIVE:QA;
console.log(process.env.NODE_ENV ==='production' && process.env.REACT_APP_MS_ENV === 'live')
export const IMAGE_CLOUD_NAME = process.env.NODE_ENV ===  'production' && process.env.REACT_APP_MS_ENV === 'live' ?'dgcojyezg':'dz4u73trs';
export const IMAGE_CLOUD_PRESET = process.env.NODE_ENV ===  'production' && process.env.REACT_APP_MS_ENV === 'live'?'hglnuurt':'xgnvzwig';
export const PDF__TRANSACTIONS_PRESET = 'pdf_transaction_preset';
export const PDF__TRANSACTIONS_CLOUDE_NAME = process.env.NODE_ENV ===  'production' && process.env.REACT_APP_MS_ENV === 'live'?'dgcojyezg':'dz4u73trs';
console.log(process.env)
// Dates
export const TIME_STRING_FORMAT = 'hh:mm A';
export const DATE_FORMAT = 'DD MMM YY';

// Messages
export const COPY_PASTE_WARNING_BODY = 'Please think twice before copy pasting. You are going to make an innocent dev soul suffer for being a dev soul. The string you paste may contain hidden characters which could cause bugs. Dont be lazy enter them manually. Its still faster than you could manually do it'
export const COPY_PASTE_WARNING_HEADING = 'COPY PASTE DETECTED! 😠😠😠😠'