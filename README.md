## Starting development

* `git clone https://bitbucket.org/millionspaces/ms-admin-react` - Clone project from bitbucket
* `yarn install` - Install dependencies
* `yarn start` - Start project


### Rewired Commands.

* `start` - Start development with HMR
* `build` - Default build
* `build-qae` - QAE Build **Recommended over default**.
* `build-live` - LIVE build
* `deploy-live` - Deploy to LIVE server 
* `deploy-qae` - Deploy to QAE server
* `bnd-live` - Build LIVE and deploy to LIVE Server
* `bnd-qae` - Build QAE and deploy to QAE Server


### Dependencies
 
* antd
* crypto-js
* google-maps-react
* react
* react-cookie
* react-dom
* react-places-autocomplete
* react-redux
* react-router-dom
* react-scripts
* react-throttle
* redux
* redux-thunk

### Dev Dependencies

* babel-plugin-import
* react-app-rewired
* redux-immutable-state-invariant
* redux-logger
* shortid

#### Latest Version
`QA: v1.0.8.18`

`LIVE: - `