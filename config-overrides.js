const { injectBabelPlugin } = require('react-app-rewired');

module.exports = function override(config, env) {
    console.log(process.env.REACT_APP_MS_ENV);
    config = injectBabelPlugin(['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }], config);
    return config;
};